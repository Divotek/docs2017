-- Valentina Studio --
-- MySQL dump --
-- ---------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- ---------------------------------------------------------


-- CREATE TABLE "meta" -------------------------------------
-- CREATE TABLE "meta" -----------------------------------------
CREATE TABLE `meta` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`slug` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`tpl_id` Int( 11 ) NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 7;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "meta_description" -------------------------
-- CREATE TABLE "meta_description" -----------------------------
CREATE TABLE `meta_description` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`meta_id` Int( 11 ) NULL,
	`name` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`description` LongText CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`meta_title` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`meta_h1` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`meta_description` LongText CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`meta_keyword` LongText CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`language_id` Int( 11 ) NOT NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `UNIQ_C52374D139FCA6F9` UNIQUE( `meta_id` ) )
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 7;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "page" -------------------------------------
-- CREATE TABLE "page" -----------------------------------------
CREATE TABLE `page` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`parent_id` Int( 11 ) NULL,
	`slug` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`status` Int( 11 ) NOT NULL,
	`date_up` DateTime NOT NULL,
	`sort` Int( 11 ) NOT NULL,
	`type_list` VarChar( 11 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
	`action` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`show_main` TinyInt( 1 ) NOT NULL,
	`show_footer` TinyInt( 1 ) NOT NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 18;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "page_description" -------------------------
-- CREATE TABLE "page_description" -----------------------------
CREATE TABLE `page_description` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`page_id` Int( 11 ) NULL,
	`name` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`description` LongText CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`meta_title` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`meta_h1` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`meta_description` LongText CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`meta_keyword` LongText CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`language_id` Int( 11 ) NOT NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `UNIQ_7A581E54C4663E4` UNIQUE( `page_id` ) )
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 17;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "role" -------------------------------------
-- CREATE TABLE "role" -----------------------------------------
CREATE TABLE `role` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`role` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`name` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 3;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "role_user" --------------------------------
-- CREATE TABLE "role_user" ------------------------------------
CREATE TABLE `role_user` ( 
	`role_id` Int( 11 ) NOT NULL,
	`user_id` Int( 11 ) NOT NULL,
	PRIMARY KEY ( `role_id`, `user_id` ) )
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
ENGINE = InnoDB;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "setting" ----------------------------------
-- CREATE TABLE "setting" --------------------------------------
CREATE TABLE `setting` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`slug` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`name` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`value` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`status` TinyInt( 1 ) NOT NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 4;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "user" -------------------------------------
-- CREATE TABLE "user" -----------------------------------------
CREATE TABLE `user` ( 
	`id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`username` VarChar( 25 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`password` VarChar( 64 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`email` VarChar( 60 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`is_active` TinyInt( 1 ) NOT NULL,
	`date_up` DateTime NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `UNIQ_8D93D649E7927C74` UNIQUE( `email` ),
	CONSTRAINT `UNIQ_8D93D649F85E0677` UNIQUE( `username` ) )
CHARACTER SET = utf8
COLLATE = utf8_unicode_ci
ENGINE = InnoDB
AUTO_INCREMENT = 4;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- Dump data of "meta" -------------------------------------
INSERT INTO `meta`(`id`,`slug`,`tpl_id`) VALUES ( '1', 'home', NULL );
INSERT INTO `meta`(`id`,`slug`,`tpl_id`) VALUES ( '2', 'registration', NULL );
INSERT INTO `meta`(`id`,`slug`,`tpl_id`) VALUES ( '3', 'login', NULL );
INSERT INTO `meta`(`id`,`slug`,`tpl_id`) VALUES ( '4', 'personal', NULL );
INSERT INTO `meta`(`id`,`slug`,`tpl_id`) VALUES ( '5', 'order-success', NULL );
INSERT INTO `meta`(`id`,`slug`,`tpl_id`) VALUES ( '6', 'order-failed', NULL );
-- ---------------------------------------------------------


-- Dump data of "meta_description" -------------------------
INSERT INTO `meta_description`(`id`,`meta_id`,`name`,`description`,`meta_title`,`meta_h1`,`meta_description`,`meta_keyword`,`language_id`) VALUES ( '1', '1', 'Домашняя страница', '', 'Главная', 'Домашняя страница', '', '', '1' );
INSERT INTO `meta_description`(`id`,`meta_id`,`name`,`description`,`meta_title`,`meta_h1`,`meta_description`,`meta_keyword`,`language_id`) VALUES ( '2', '2', 'Страница регистрации пользователя', '', 'Регистрация', 'Страница регистрации пользователя', '', '', '1' );
INSERT INTO `meta_description`(`id`,`meta_id`,`name`,`description`,`meta_title`,`meta_h1`,`meta_description`,`meta_keyword`,`language_id`) VALUES ( '3', '3', 'Страница входа пользователя', '', 'Вход', 'Страница входа пользователя', '', '', '1' );
INSERT INTO `meta_description`(`id`,`meta_id`,`name`,`description`,`meta_title`,`meta_h1`,`meta_description`,`meta_keyword`,`language_id`) VALUES ( '4', '4', 'Личный кабинет', '', 'Личный кабинет', '', '', '', '1' );
INSERT INTO `meta_description`(`id`,`meta_id`,`name`,`description`,`meta_title`,`meta_h1`,`meta_description`,`meta_keyword`,`language_id`) VALUES ( '5', '5', 'Страница успешного завершения заказа', '', 'Успешный заказ !', '', '', '', '1' );
INSERT INTO `meta_description`(`id`,`meta_id`,`name`,`description`,`meta_title`,`meta_h1`,`meta_description`,`meta_keyword`,`language_id`) VALUES ( '6', '6', 'Страница неудачного заказа', '', 'Ваш заказ не удался !', '', '', '', '1' );
-- ---------------------------------------------------------


-- Dump data of "page" -------------------------------------
INSERT INTO `page`(`id`,`parent_id`,`slug`,`status`,`date_up`,`sort`,`type_list`,`action`,`show_main`,`show_footer`) VALUES ( '13', NULL, 'about-us', '1', '2017-10-25 10:32:04', '1', 'link', 'page_show', '1', '0' );
INSERT INTO `page`(`id`,`parent_id`,`slug`,`status`,`date_up`,`sort`,`type_list`,`action`,`show_main`,`show_footer`) VALUES ( '14', NULL, 'contacts', '1', '2017-10-23 19:41:35', '2', 'link', 'page_show', '1', '0' );
INSERT INTO `page`(`id`,`parent_id`,`slug`,`status`,`date_up`,`sort`,`type_list`,`action`,`show_main`,`show_footer`) VALUES ( '15', NULL, 'our-services', '1', '2017-10-23 19:44:51', '3', 'link', 'page_show', '1', '0' );
INSERT INTO `page`(`id`,`parent_id`,`slug`,`status`,`date_up`,`sort`,`type_list`,`action`,`show_main`,`show_footer`) VALUES ( '16', '15', 'service-1', '1', '2017-10-25 10:32:09', '4', 'link', 'page_show', '0', '0' );
INSERT INTO `page`(`id`,`parent_id`,`slug`,`status`,`date_up`,`sort`,`type_list`,`action`,`show_main`,`show_footer`) VALUES ( '17', '15', 'service-2', '1', '2017-10-23 19:46:43', '5', 'link', 'page_show', '0', '0' );
-- ---------------------------------------------------------


-- Dump data of "page_description" -------------------------
INSERT INTO `page_description`(`id`,`page_id`,`name`,`description`,`meta_title`,`meta_h1`,`meta_description`,`meta_keyword`,`language_id`) VALUES ( '12', '13', 'О нас', '<p>Building a website is, in many ways, an exercise of willpower. It&rsquo;s tempting to get distracted by the bells and whistles of the design process, and forget all about creating compelling content. But it&#39;s that last part that&#39;s crucial to making inbound marketing work for your business.</p>

<p>So how do you balance your remarkable content creation with your web design needs? It all starts with the &quot;About Us&quot; page.</p>

<p>For a remarkable about page, all you need to do is figure out your company&#39;s unique identity, and then share it with the world. Easy, right? Of course it&#39;s not easy. That said, the &quot;About Us&quot; page is one of the most important pages on your website, and it can&#39;t go neglected. It also happens to be one of the most commonly overlooked pages, which is why you should make it stand out.</p>', '', '', '', '', '1' );
INSERT INTO `page_description`(`id`,`page_id`,`name`,`description`,`meta_title`,`meta_h1`,`meta_description`,`meta_keyword`,`language_id`) VALUES ( '13', '14', 'Контакты', '<p>Building a website is, in many ways, an exercise of willpower. It&rsquo;s tempting to get distracted by the bells and whistles of the design process, and forget all about creating compelling content. But it&#39;s that last part that&#39;s crucial to making inbound marketing work for your business.</p>

<p>So how do you balance your remarkable content creation with your web design needs? It all starts with the &quot;About Us&quot; page.</p>

<p>For a remarkable about page, all you need to do is figure out your company&#39;s unique identity, and then share it with the world. Easy, right? Of course it&#39;s not easy. That said, the &quot;About Us&quot; page is one of the most important pages on your website, and it can&#39;t go neglected. It also happens to be one of the most commonly overlooked pages, which is why you should make it stand out.</p>', '', '', '', '', '1' );
INSERT INTO `page_description`(`id`,`page_id`,`name`,`description`,`meta_title`,`meta_h1`,`meta_description`,`meta_keyword`,`language_id`) VALUES ( '14', '15', 'Наши услуги', '', '', '', '', '', '1' );
INSERT INTO `page_description`(`id`,`page_id`,`name`,`description`,`meta_title`,`meta_h1`,`meta_description`,`meta_keyword`,`language_id`) VALUES ( '15', '16', 'Услуга 1', '', '', '', '', '', '1' );
INSERT INTO `page_description`(`id`,`page_id`,`name`,`description`,`meta_title`,`meta_h1`,`meta_description`,`meta_keyword`,`language_id`) VALUES ( '16', '17', 'Услуга-2', '', '', '', '', '', '1' );
-- ---------------------------------------------------------


-- Dump data of "role" -------------------------------------
INSERT INTO `role`(`id`,`role`,`name`) VALUES ( '2', 'ROLE_USER', 'Пользователь' );
INSERT INTO `role`(`id`,`role`,`name`) VALUES ( '3', 'ROLE_ADMIN', 'Администратор' );
-- ---------------------------------------------------------


-- Dump data of "role_user" --------------------------------
INSERT INTO `role_user`(`role_id`,`user_id`) VALUES ( '2', '3' );
INSERT INTO `role_user`(`role_id`,`user_id`) VALUES ( '3', '1' );
-- ---------------------------------------------------------


-- Dump data of "setting" ----------------------------------
-- ---------------------------------------------------------


-- Dump data of "user" -------------------------------------
INSERT INTO `user`(`id`,`username`,`password`,`email`,`is_active`,`date_up`) VALUES ( '1', 'admin', '$2y$13$bQqk6.p84EHujbnkSK8ZK.KxmrcVh8CgXJK5DuAGQ3nFh1Lf9ydUm', 'alex073507@gmail.com', '1', '2017-10-23 19:40:09' );
INSERT INTO `user`(`id`,`username`,`password`,`email`,`is_active`,`date_up`) VALUES ( '3', 'Alex', '$2y$13$Z81AjVlYH8orCLA3VMqMlugJUuJOzzSnLVgj/77PKM7es.RuSE11C', 'sawwwn@gmail.com', '1', '2017-10-24 19:25:01' );
-- ---------------------------------------------------------


-- CREATE INDEX "IDX_140AB620727ACA70" ---------------------
-- CREATE INDEX "IDX_140AB620727ACA70" -------------------------
CREATE INDEX `IDX_140AB620727ACA70` USING BTREE ON `page`( `parent_id` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "IDX_332CA4DDA76ED395" ---------------------
-- CREATE INDEX "IDX_332CA4DDA76ED395" -------------------------
CREATE INDEX `IDX_332CA4DDA76ED395` USING BTREE ON `role_user`( `user_id` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "IDX_332CA4DDD60322AC" ---------------------
-- CREATE INDEX "IDX_332CA4DDD60322AC" -------------------------
CREATE INDEX `IDX_332CA4DDD60322AC` USING BTREE ON `role_user`( `role_id` );
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE LINK "FK_C52374D139FCA6F9" -----------------------
-- CREATE LINK "FK_C52374D139FCA6F9" ---------------------------
ALTER TABLE `meta_description`
	ADD CONSTRAINT `FK_C52374D139FCA6F9` FOREIGN KEY ( `meta_id` )
	REFERENCES `meta`( `id` )
	ON DELETE Cascade
	ON UPDATE No Action;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE LINK "FK_140AB620727ACA70" -----------------------
-- CREATE LINK "FK_140AB620727ACA70" ---------------------------
ALTER TABLE `page`
	ADD CONSTRAINT `FK_140AB620727ACA70` FOREIGN KEY ( `parent_id` )
	REFERENCES `page`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE LINK "FK_7A581E54C4663E4" ------------------------
-- CREATE LINK "FK_7A581E54C4663E4" ----------------------------
ALTER TABLE `page_description`
	ADD CONSTRAINT `FK_7A581E54C4663E4` FOREIGN KEY ( `page_id` )
	REFERENCES `page`( `id` )
	ON DELETE Cascade
	ON UPDATE No Action;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE LINK "FK_332CA4DDA76ED395" -----------------------
-- CREATE LINK "FK_332CA4DDA76ED395" ---------------------------
ALTER TABLE `role_user`
	ADD CONSTRAINT `FK_332CA4DDA76ED395` FOREIGN KEY ( `user_id` )
	REFERENCES `user`( `id` )
	ON DELETE Cascade
	ON UPDATE Restrict;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE LINK "FK_332CA4DDD60322AC" -----------------------
-- CREATE LINK "FK_332CA4DDD60322AC" ---------------------------
ALTER TABLE `role_user`
	ADD CONSTRAINT `FK_332CA4DDD60322AC` FOREIGN KEY ( `role_id` )
	REFERENCES `role`( `id` )
	ON DELETE Cascade
	ON UPDATE Restrict;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


