<?php

namespace AppBundle\Form\Model;

use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;
use Symfony\Component\Validator\Constraints as Assert;

class ChangeEmailModel
{
    /**
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     */
    private $email = '';
     
    public function setEmail($email){
        $this->email = $email;
        return $this;
    }
    
    public function getEmail(){
        return $this->email;
    }
}