<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Meta
 *
 * @ORM\Table(name="code")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CodeRepository")
 */
class Code
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * name="value", type="string"
     */
    private $value = '';
    
    /**
     * One Code has Many Fields.
     * @ORM\OneToMany(targetEntity="Field", mappedBy="code")
     */
    private $fields;

    /*GETTERS METHODS*/

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        if($this->id){
            $this->value = '[[CODE:' . $this->id . ']]';
        }
        return $this->value;
    }
   
    /*END GETTERS METHODS*/

    /*SETTER METHODS*/

    /**
     * Set value
     *
     * @param string value
     *
     * @return Code
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /*END SETTER METHODS*/

    /*CONSTRUCT METHOD*/
    function __construct()
    {
       $this->fields = new ArrayCollection();
    }

}

