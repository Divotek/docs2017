<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\CategoryDescription;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="parent_id", type="integer", nullable=true, length=11)
     */

    private $parent_id;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     */
    private $slug = '';

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", length=11)
     */
    private $status = 1;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_up", type="datetime")
     */
    private $date_up;

    /**
     * @var string
     *
     * @ORM\Column(name="type_list", type="string", nullable=true, length=11)
     */

    private $type_list = 'link';

    /**
     * @var int
     *
     * @ORM\Column(name="sort", type="integer", length=11)
     */

    private $sort = 0;

    /**
     * One Category has Many Categories.
     * @ORM\OneToMany(targetEntity="Category", mappedBy="parent")
     * @ORM\OrderBy({"sort" = "ASC"})
     */
    private $children;

    /**
     * Many Categories have One category.
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;


    /**
     * @ORM\OneToOne(targetEntity="CategoryDescription", mappedBy="category", cascade={"All"})
     */
    private $description;

    /**
     * @ORM\OneToOne(targetEntity="Page", mappedBy="category")
     */
    private $page;

    /**
     * Many Categories have Many Documents.
     * @ORM\ManyToMany(targetEntity="Document", mappedBy="categories")
     * @ORM\OrderBy({"sort" = "ASC"})
     */
    private $documents;

    /*GETTERS METHODS*/

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get dateUp
     *
     * @return \DateTime
     */
    public function getDateUp()
    {
        return $this->date_up;
    }

    /**
     * Get typeList
     *
     * @return \string
     */
    public function getTypeList()
    {
        return $this->type_list;
    }

    /**
     * Get sort
     *
     * @return \int
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * Get children
     *
     * @return ArrayCollection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Get parent
     *
     * @return Category
     */

    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Get description
     *
     * @return CategoryDescription
     */

    public function getDescription() {
        return $this->description;
    }

    /**
     * Get documents
     *
     * @return ArrayCollection
     */

    public function getDocuments() {
        return $this->documents;
    }
    /*END GETTERS METHODS*/

    /*SETTER METHODS*/

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Category
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Category
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Set dateUp
     *
     * @param \DateTime $dateUp
     *
     * @return Category
     */
    public function setDateUp($dateUp)
    {
        $this->date_up = $dateUp;

        return $this;
    }

    /**
     * Set typeList
     *
     * @param \int $typeList
     *
     * @return Category
     */
    public function setTypeList($typeList)
    {
        $this->type_list = $typeList;

        return $this;
    }

    /**
     * Set sort
     *
     * @param \int $sort
     *
     * @return Category
     */
    public function setSort($sort)
    {
        $this->sort = $sort;
        return $this;
    }

    /**
     * Set parent
     *
     * @return Category
     */
    public function setParent($category)
    {
        $this->parent = $category;
        return $this;
    }

    /**
     * Set description
     *
     * @return Category
     */
    public function setDescription(CategoryDescription $description) {
        $this->description = $description;
        return $this;
    }
    /*END SETTER METHODS*/

    /*CONSTRUCT METHOD*/
    function __construct()
    {
        $this->children     = new ArrayCollection();
        $this->description  = new CategoryDescription();
        $this->documents    = new ArrayCollection();
        $this->date_up      = new \DateTime("now");
    }

    public function __toString()
    {
        if($this->getDescription()->getName()){
            return $this->getDescription()->getName();
        }
        return 'Category';
    }

}

