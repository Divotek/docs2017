<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Entity\DocumentDescription;
use AppBundle\Entity\Order;

/**
 * Document
 *
 * @ORM\Table(name="document")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DocumentRepository")
 */
class Document
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer", length=11)
     */
    private $user_id;

    /**
     * @var int
     *
     * @ORM\Column(name="parent_id", type="integer", nullable=true, length=11)
     */

    private $parent_id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;
    
    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", nullable=true, length=255)
     */
    private $link;
            
    /**
    * @var float
    * 
    * @ORM\Column(type="decimal", precision=15, scale=2)
    */
    private $price = 0;
    
    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     */
    private $slug = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_up", type="datetime")
     */
    private $date_up;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", length=11)
     */
    private $status = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="sort", type="integer", length=11)
     */

    private $sort = 0;


    /**
     * @ORM\OneToOne(targetEntity="DocumentDescription", mappedBy="document", cascade={"all"})
     */
    private $description;
    
    /**
     * @ORM\OneToOne(targetEntity="Order", mappedBy="document", cascade={"all"})
     */
    private $order;
    
    /**
     * Many Documents have Many Categories.
     * @ORM\ManyToMany(targetEntity="Category", inversedBy="documents")
     * @ORM\OrderBy({"sort" = "ASC"})
     * @ORM\JoinTable(name="category_document")
     */
    private $categories;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="documents")
     */
    private $user;

    /**
     * Many Document have Many Fields.
     * @ORM\OneToMany(targetEntity="Field", mappedBy="document", cascade={"All"})
     * @ORM\OrderBy({"sort" = "ASC"})
     */
    private $fields;


    /**
     * One Document has Many Documents.
     * @ORM\OneToMany(targetEntity="Document", mappedBy="parent")
     */
    private $children;

    /**
     * Many Documents have One Document.
     * @ORM\ManyToOne(targetEntity="Document", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;


    function __construct()
    {
        $this->description  = new DocumentDescription();
        $this->categories   = new ArrayCollection();
        $this->fields       = new ArrayCollection();
        $this->children     = new ArrayCollection();
        $this->date_up      = new \DateTime("now");
    }

    public function setDescription(DocumentDescription $description) {
        $this->description = $description;
    }

    public function addCategory(Category $category) {
        $this->categories[] = $category;
    }

    public function addField(Field $field) {
        $this->fields[] = $field;
    }

    public function getDescription() {
        return $this->description;
    }
    
    public function getCategories() {
        return $this->categories;
    }

    public function getFields() {
        return $this->fields;
    }
    
    public function getOrder() {
        return $this->order;
    }

    public function setOrder(Order $order) {
        $this->order = $order;
    }

    public function getUser() {
        return $this->user;
    }

    public function setUser(User $user) {
        $this->user = $user;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function setParent($document)
    {
        $this->parent = $document;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateUp
     *
     * @param \DateTime $dateUp
     *
     * @return Document
     */
    public function setDateUp($dateUp)
    {
        $this->date_up = $dateUp;

        return $this;
    }

    /**
     * Get dateUp
     *
     * @return \DateTime
     */
    public function getDateUp()
    {
        return $this->date_up;
    }
    
    /**
     * Get dateUp
     *
     * @return \string
     */
    public function getDateFront()
    {
        return $this->date_up->format('d.m.Y H:i:s');
    }

    /**
     * Set sort
     *
     * @param \int $sort
     *
     * @return Document
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Get sort
     *
     * @return \int
     */
    public function getSort()
    {
        return $this->sort;
    }
    
    /**
     * Set price
     *
     * @param \float $price
     *
     * @return Document
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return \price
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Document
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Document
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Document
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }
    
    

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * Set link
     *
     * @param string $link
     *
     * @return Document
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }
    
    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }
}    