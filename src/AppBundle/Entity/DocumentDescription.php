<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DocumentDescription
 *
 * @ORM\Table(name="document_description")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DocumentDescriptionRepository")
 */
class DocumentDescription
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="document_id", type="integer", nullable=true, length=11)
     */
    private $document_id;

    /**
     * @var int
     *
     * @ORM\Column(name="language_id", type="integer", length=11)
     */
    private $language_id = 1;

    /**
     * @var string,
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string,
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string,
     *
     * @ORM\Column(name="short_description", type="text")
     */
    private $short_description;

    /**
     * @var string,
     *
     * @ORM\Column(name="meta_description", type="text")
     */
    private $meta_description;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_h1", type="string", length=255)
     */
    private $meta_h1;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_keyword", type="string", length=255)
     */
    private $meta_keyword;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_title", type="string", length=255)
     */
    private $meta_title;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set documentId
     *
     * @param integer $documentId
     *
     * @return DocumentDescription
     */
    public function setDocumentId($documentId)
    {
        $this->document_id = $documentId;

        return $this;
    }

    /**
     * Get documentId
     *
     * @return int
     */
    public function getDocumentId()
    {
        return $this->document_id;
    }

    /**
     * Set languageId
     *
     * @param integer $languageId
     *
     * @return DocumentDescription
     */
    public function setLanguageId($languageId)
    {
        $this->language_id = $languageId;

        return $this;
    }

    /**
     * Get languageId
     *
     * @return int
     */
    public function getLanguageId()
    {
        return $this->language_id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return DocumentDescription
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set short_description
     *
     * @param string $short_description
     *
     * @return DocumentDescription
     */
    public function setShortDescription($short_description)
    {
        $this->short_description = $short_description;

        return $this;
    }

    /**
     * Get short_description
     *
     * @return string
     */
    public function getShortDescription()
    {
        return $this->short_description;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return DocumentDescription
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set metaDescription
     *
     * @param string $metaDescription
     *
     * @return DocumentDescription
     */
    public function setMetaDescription($metaDescription)
    {
        $this->meta_description = $metaDescription;

        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->meta_description;
    }

    /**
     * Set metaH1
     *
     * @param string $metaH1
     *
     * @return DocumentDescription
     */
    public function setMetaH1($metaH1)
    {
        $this->meta_h1 = $metaH1;

        return $this;
    }

    /**
     * Get metaH1
     *
     * @return string
     */
    public function getMetaH1()
    {
        return $this->meta_h1;
    }

    /**
     * Set metaKeyword
     *
     * @param string $metaKeyword
     *
     * @return DocumentDescription
     */
    public function setMetaKeyword($metaKeyword)
    {
        $this->meta_keyword = $metaKeyword;

        return $this;
    }

    /**
     * Get metaKeyword
     *
     * @return string
     */
    public function getMetaKeyword()
    {
        return $this->meta_keyword;
    }

    /**
     * Set metaTitle
     *
     * @param string $metaTitle
     *
     * @return DocumentDescription
     */
    public function setMetaTitle($metaTitle)
    {
        $this->meta_title = $metaTitle;

        return $this;
    }

    /**
     * Get metaTitle
     *
     * @return string
     */
    public function getMetaTitle()
    {
        return $this->meta_title;
    }

    /**
     * @ORM\OneToOne(targetEntity="Document", inversedBy="description")
     * @ORM\JoinColumn(name="document_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $document;

    public function setDocument(Document $document)
    {
        $this->document = $document;
    }

    public function getDocument()
    {
        return $this->document;
    }
    
    public function __clone() 
    {
        $this->id           = NULL;
        $this->document_id  = NULL;
        $this->document     = NULL;
    }
}

