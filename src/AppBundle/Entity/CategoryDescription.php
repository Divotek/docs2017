<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CategoryDescription
 *
 * @ORM\Table(name="category_description")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoryDescriptionRepository")
 */
class CategoryDescription
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="category_id", type="integer", length=11)
     */
    private $category_id;

    /**
     * @var int
     *
     * @ORM\Column(name="language_id", type="integer", length=11)
     */
    private $language_id = 1;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string,
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;


    /**
     * @var string,
     *
     * @ORM\Column(name="meta_description", type="text")
     */
    private $meta_description;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_h1", type="string", length=255)
     */
    private $meta_h1;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_keyword", type="string", length=255)
     */
    private $meta_keyword;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_title", type="string", length=255)
     */
    private $meta_title;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set categoryId
     *
     * @param integer $categoryId
     *
     * @return CategoryDescription
     */
    public function setCategoryId($categoryId)
    {
        $this->category_id = $categoryId;

        return $this;
    }

    /**
     * Get categoryId
     *
     * @return int
     */
    public function getCategoryId()
    {
        return $this->category_id;
    }

    /**
     * Set languageId
     *
     * @param integer $languageId
     *
     * @return CategoryDescription
     */
    public function setLanguageId($languageId)
    {
        $this->language_id = $languageId;

        return $this;
    }

    /**
     * Get languageId
     *
     * @return int
     */
    public function getLanguageId()
    {
        return $this->language_id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CategoryDescription
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @ORM\OneToOne(targetEntity="Category", inversedBy="description")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $category;

    public function setCategory(Category $category)
    {
        $this->category = $category;
    }

    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return CategoryDescription
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set metaDescription
     *
     * @param string $metaDescription
     *
     * @return CategoryDescription
     */
    public function setMetaDescription($metaDescription)
    {
        $this->meta_description = $metaDescription;

        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->meta_description;
    }

    /**
     * Set metaH1
     *
     * @param string $metaH1
     *
     * @return CategoryDescription
     */
    public function setMetaH1($metaH1)
    {
        $this->meta_h1 = $metaH1;

        return $this;
    }

    /**
     * Get metaH1
     *
     * @return string
     */
    public function getMetaH1()
    {
        return $this->meta_h1;
    }

    /**
     * Set metaKeyword
     *
     * @param string $metaKeyword
     *
     * @return CategoryDescription
     */
    public function setMetaKeyword($metaKeyword)
    {
        $this->meta_keyword = $metaKeyword;

        return $this;
    }

    /**
     * Get metaKeyword
     *
     * @return string
     */
    public function getMetaKeyword()
    {
        return $this->meta_keyword;
    }

    /**
     * Set metaTitle
     *
     * @param string $metaTitle
     *
     * @return CategoryDescription
     */
    public function setMetaTitle($metaTitle)
    {
        $this->meta_title = $metaTitle;

        return $this;
    }

    /**
     * Get metaTitle
     *
     * @return string
     */
    public function getMetaTitle()
    {
        return $this->meta_title;
    }
}

