<?php
// src/AppBundle/Entity/User.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Entity\Role;

/**
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $username;
    
    /**
     * @Assert\NotBlank()
     * @Assert\Length(max=4096)
     */
    private $plainPassword;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=60, unique=true)
     */
    private $email = '';

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive = true;

    /**
     * Many Users have Many Roles.
     * @ORM\ManyToMany(targetEntity="Role", mappedBy="users")
     */
    private $roles;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_up", nullable=true, type="datetime")
     */
    private $date_up;
    
    /*Changed id roles*/
    private $changed = [];
    
    public function __construct()
    {
        $this->isActive = true;
        $this->roles    = new ArrayCollection();
        $this->roles_admin = new ArrayCollection();
        $this->date_up  = new \DateTime("now");
        // may not be needed, see section on salt below
        // $this->salt = md5(uniqid(null, true));
    }

    public function getId()
    {
        return $this->id;
    }
    
    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getUsername()
    {
        return $this->username;
    }
    
    public function setUsername($username)
    {
        $this->username = $username;
    }
    
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }

    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getRoles()
    {
        $Roles = $this->roles->toArray();
        $result = [];
        if(count($Roles) > 0){
            foreach ($Roles as $Role){
                $result[] = $Role->getRole();
            }
            return $result;
        }
        return NULL;
    }
    
    public function getUserRoles()
    {
        return $this->roles->toArray();
    }
    
    public function removeRole(Role $Role)
    {
        $this->roles->removeElement($Role);
    }
    
    public function getStringRoles()
    {
        $Roles = $this->roles->toArray();
        $result = '';
        if(count($Roles) > 0){
            foreach ($Roles as $Role){
                $result .= $Role->getName() . ' ';
            }
        }
        return $result;
    }
    
    public function getFormRoles()
    {
        $Roles = $this->roles->toArray();
        $result = [];
        if(count($Roles) > 0){
            foreach ($Roles as $Role){
                $result[$Role->getName()] = $Role->getId();
            }
        }
       return $result;
    }
        
    public function addRole(Role $Role)
    {
        $this->roles[] = $Role;
        return $this;
    }
    
        
    public function setChanged(array $ar)
    {
        $this->changed = $ar;
        return $this;
    }
    
    public function getChanged()
    {
        return $this->changed;
    }
    
    public function addChanged($ar)
    {
        $this->changed[] = $ar;
        return $this;
    }

    public function eraseCredentials()
    {
    }
    
    /**
     * Get dateUp
     *
     * @return \DateTime
     */
    public function getDateUp()
    {
        return $this->date_up;
    }
    
    /**
     * Set dateUp
     *
     * @param \DateTime $dateUp
     *
     * @return User
     */
    public function setDateUp($dateUp)
    {
        $this->date_up = $dateUp;

        return $this;
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            // see section on salt below
            // $this->salt
        ) = unserialize($serialized);
    }
    
    public function getPersonal(){
        return 'personal';
    }

        public function __toString() {
        return $this->username;
    }
}