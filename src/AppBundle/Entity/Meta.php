<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Entity\MetaDescription;

/**
 * Meta
 *
 * @ORM\Table(name="meta")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MetaRepository")
 */
class Meta
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     */
    private $slug = '';


    /**
     * @var int
     *
     * @ORM\Column(name="tpl_id", type="integer", nullable=true, length=11)
     */
    private $tpl_id;

    /**
     * @ORM\OneToOne(targetEntity="MetaDescription", mappedBy="meta", cascade={"All"})
     */
    private $description;

    /*GETTERS METHODS*/

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Get tplId
     *
     * @return \int
     */
    public function getTplId()
    {
        return $this->tpl_id;
    }

    /**
     * Get description
     *
     * @return MetaDescription
     */
    public function getDescription() {
        return $this->description;
    }
    /*END GETTERS METHODS*/

    /*SETTER METHODS*/

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Meta
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Set tplId
     *
     * @param \int $tplId
     *
     * @return Meta
     */
    public function setTplId($tplId)
    {
        $this->tpl_id = $tplId;

        return $this;
    }

    /**
     * Set description
     *
     * @return Meta
     */
    public function setDescription(MetaDescription $description) {
        $this->description = $description;
        return $this;
    }
    /*END SETTER METHODS*/

    /*CONSTRUCT METHOD*/
    public function __construct()
    {
        $this->description  = new MetaDescription();
    }


}

