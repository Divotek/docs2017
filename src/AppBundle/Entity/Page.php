<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Entity\PageDescription;

/**
 * Page
 *
 * @ORM\Table(name="page")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PageRepository")
 */
class Page
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="parent_id", type="integer", nullable=true, length=11)
     */

    private $parent_id;

    /**
     * @var int
     *
     * @ORM\Column(name="category_id", type="integer", nullable=true, length=11)
     */

    private $category_id;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     */
    private $slug = '';

    /**
     * @var string
     *
     * @ORM\Column(name="action", type="string", length=255)
     */
    private $action = 'page_show';

    /**
     * @var bool
     *
     * @ORM\Column(name="show_main", type="boolean")
     */
    private $show_main = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="show_footer", type="boolean")
     */
    private $show_footer = false;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", length=11)
     */
    private $status = 1;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_up", type="datetime")
     */
    private $date_up;

    /**
     * @var string
     *
     * @ORM\Column(name="type_list", type="string", nullable=true, length=11)
     */

    private $type_list = 'link';

    /**
     * @var int
     *
     * @ORM\Column(name="sort", type="integer", length=11)
     */

    private $sort = 0;

    /**
     * One Page has Many Pages.
     * @ORM\OneToMany(targetEntity="Page", mappedBy="parent")
     */
    private $children;

    /**
     * Many Pages have One Page.
     * @ORM\ManyToOne(targetEntity="Page", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;

    /**
     * One Page have One Category.
     * @ORM\OneToOne(targetEntity="Category", inversedBy="page")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @ORM\OneToOne(targetEntity="PageDescription", mappedBy="page", cascade={"All"})
     */
    private $description;

    /*GETTERS METHODS*/

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Get action
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Get show_main
     *
     * @return bool
     */
    public function getShowMain()
    {
        return $this->show_main;
    }

    /**
     * Get show_footer
     *
     * @return bool
     */
    public function getShowFooter()
    {
        return $this->show_footer;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }


    /**
     * Get dateUp
     *
     * @return \DateTime
     */
    public function getDateUp()
    {
        return $this->date_up;
    }

    /**
     * Get typeList
     *
     * @return \string
     */
    public function getTypeList()
    {
        return $this->type_list;
    }

    /**
     * Get sort
     *
     * @return \int
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * Get children
     *
     * @return ArrayCollection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Get parent
     *
     * @return Page
     */

    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Get parent
     *
     * @return Category
     */

    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Get description
     *
     * @return PageDescription
     */

    public function getDescription() {
        return $this->description;
    }
    /*END GETTERS METHODS*/

    /*SETTER METHODS*/

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Page
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Set parent
     *
     * @return Page
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Set showMain
     *
     * @param bool $showMain
     *
     * @return Page
     */
    public function setShowMain($showMain)
    {
        $this->show_main = $showMain;

        return $this;
    }

    /**
     * Set showFooter
     *
     * @param bool $showFooter
     *
     * @return Page
     */
    public function setShowFooter($showFooter)
    {
        $this->show_footer = $showFooter;

        return $this;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Page
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Set dateUp
     *
     * @param \DateTime $dateUp
     *
     * @return Page
     */
    public function setDateUp($dateUp)
    {
        $this->date_up = $dateUp;

        return $this;
    }

    /**
     * Set typeList
     *
     * @param \int $typeList
     *
     * @return Page
     */
    public function setTypeList($typeList)
    {
        $this->type_list = $typeList;

        return $this;
    }

    /**
     * Set sort
     *
     * @param \int $sort
     *
     * @return Page
     */
    public function setSort($sort)
    {
        $this->sort = $sort;
        return $this;
    }

    /**
     * Set parent
     *
     * @return Page
     */
    public function setParent($page)
    {
        $this->parent = $page;

        return $this;
    }

    /**
     * Set parent
     *
     * @return Page
     */
    public function setCategory($category)
    {
        $this->category = $category;
        $this->action  = 'page_show';
        if($category){
            $this->action  = 'category_show';
            $this->slug    = $category->getSlug();
        }
        return $this;
    }

    /**
     * Set description
     *
     * @return Page
     */
    public function setDescription(PageDescription $description) {
        $this->description = $description;
        return $this;
    }

    /*END SETTER METHODS*/

    /*CONSTRUCT METHOD*/
    public function __construct()
    {
        $this->children     = new ArrayCollection();
        $this->description  = new PageDescription();
        $this->date_up      = new \DateTime("now");
    }

    public function __toString()
    {
        if($this->getDescription()->getName()){
            return $this->getDescription()->getName();
        }
        return 'Page';
    }
}

