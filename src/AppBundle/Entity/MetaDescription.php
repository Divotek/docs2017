<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MetaDescription
 *
 * @ORM\Table(name="meta_description")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MetaDescriptionRepository")
 */
class MetaDescription
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="meta_id", type="integer", nullable=true, length=11)
     */
    private $meta_id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name = '';

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description = '';

    /**
     * @var string
     *
     * @ORM\Column(name="meta_title", type="string")
     */
    private $meta_title = '';

    /**
     * @var string
     *
     * @ORM\Column(name="meta_h1", type="string", length=255)
     */
    private $meta_h1 = '';

    /**
     * @var string
     *
     * @ORM\Column(name="meta_description", type="text")
     */
    private $meta_description = '';

    /**
     * @var string
     *
     * @ORM\Column(name="meta_keyword", type="text")
     */
    private $meta_keyword = '';

    /**
     * @var int
     *
     * @ORM\Column(name="language_id", type="integer", length=11)
     */
    private $language_id = 1;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return MetaDescription
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return MetaDescription
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set metaTitle
     *
     * @param string $metaTitle
     *
     * @return MetaDescription
     */
    public function setMetaTitle($metaTitle)
    {
        $this->meta_title = $metaTitle;

        return $this;
    }

    /**
     * Get metaTitle
     *
     * @return string
     */
    public function getMetaTitle()
    {
        return $this->meta_title;
    }

    /**
     * Set metaH1
     *
     * @param string $metaH1
     *
     * @return MetaDescription
     */
    public function setMetaH1($metaH1)
    {
        $this->meta_h1 = $metaH1;

        return $this;
    }

    /**
     * Get metaH1
     *
     * @return string
     */
    public function getMetaH1()
    {
        return $this->meta_h1;
    }

    /**
     * Set metaDescription
     *
     * @param string $metaDescription
     *
     * @return MetaDescription
     */
    public function setMetaDescription($metaDescription)
    {
        $this->meta_description = $metaDescription;

        return $this;
    }

    /**
     * Get metaDescription
     *
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->meta_description;
    }

    /**
     * Set metaKeyword
     *
     * @param string $metaKeyword
     *
     * @return MetaDescription
     */
    public function setMetaKeyword($metaKeyword)
    {
        $this->meta_keyword = $metaKeyword;

        return $this;
    }

    /**
     * Get metaKeyword
     *
     * @return string
     */
    public function getMetaKeyword()
    {
        return $this->meta_keyword;
    }

    /**
     * Set languageId
     *
     * @param int $languageId
     *
     * @return MetaDescription
     */
    public function setLanguageId($languageId)
    {
        $this->language_id = $languageId;

        return $this;
    }

    /**
     * Get languageId
     *
     * @return int
     */
    public function getLanguageId()
    {
        return $this->language_id;
    }

    /**
     * @ORM\OneToOne(targetEntity="Meta", inversedBy="description")
     * @ORM\JoinColumn(name="meta_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $meta;

    public function setMeta(Meta $meta)
    {
        $this->meta = $meta;
    }

    public function getMeta()
    {
        return $this->meta;
    }


}

