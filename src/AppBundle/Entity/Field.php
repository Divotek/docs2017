<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Entity\FieldDescription;
use AppBundle\Entity\Code;

/**
 * Field
 *
 * @ORM\Table(name="field")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FieldRepository")
 */
class Field
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="document_id", nullable=true, type="integer", length=11)
     */
    private $document_id;

    /**
     * @var int
     *
     * @ORM\Column(name="parent_id", type="integer", nullable=true, length=11)
     */

    private $parent_id;

    /**
     * @var int
     *
     * @ORM\Column(name="group_id", type="integer", nullable=true, length=11)
     */
    private $group_id;
    
    /**
     * @var int
     *
     * @ORM\Column(name="code_id", type="integer", nullable=true, length=11)
     */
    private $code_id;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status = 1;

    /**
     * @var bool
     *
     * @ORM\Column(name="choice", type="boolean")
     */
    private $choice = false;

    /**
     * bool
     *
     * name="delete", type="boolean"
     */
    private $delete = false;
    
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;
    
    /**
     * @var string
     *
     * @ORM\Column(name="value", type="text")
     */
    private $value = '';

    /**
     * @var int
     *
     * @ORM\Column(name="sort", type="integer", length=11)
     */

    private $sort = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_up", type="datetime")
     */
    private $date_up;

    /**
     * Many Fields have One Documents.
     * @ORM\ManyToOne(targetEntity="Document", inversedBy="fields")
     * @ORM\JoinColumn(name="document_id", referencedColumnName="id")
     */
    private $document;

    /**
     * One Field has One Field.
     * @ORM\OneToOne(targetEntity="Field", mappedBy="parent")
     */
    private $child;

    /**
     * One Field have One Field.
     * @ORM\OneToOne(targetEntity="Field", inversedBy="child", cascade={"All"})
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * One Field has Many Fields.
     * @ORM\OneToMany(targetEntity="Field", mappedBy="group", cascade={"All"})
     */
    private $elements;

    /**
     * Many Fields have One Field.
     * @ORM\ManyToOne(targetEntity="Field", inversedBy="elements", cascade={"All"})
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $group;
    
    /**
     * Many Fields have One Code.
     * @ORM\ManyToOne(targetEntity="Code", inversedBy="fields", cascade={"persist"})
     * @ORM\JoinColumn(name="code_id", referencedColumnName="id")
     */
    private $code;
    
    /**
     * @ORM\OneToOne(targetEntity="FieldDescription", mappedBy="field", cascade={"All"})
     */
    private $description;

    /*GETTERS METHODS*/

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Get group_id
     *
     * @return int
     */
    public function getGroupId()
    {
        return $this->group_id;
    }

    /**
     * Get document
     *
     * @return Document
     */
    public function getDocument() {
        return $this->document;
    }

    /**
     * Get parent
     *
     * @return Field
     */
    public function getParent()
    {
        return $this->parent;
    }
    
    /**
     * Get parent_id
     *
     * @return Field
     */
    public function getParentId()
    {
        return $this->parent_id;
    }

    /**
     * Get group
     *
     * @return Field
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get choice
     *
     * @return \int
     */
    public function getChoice()
    {
        return $this->choice;
    }

    /**
     * Get delete
     *
     * @return \bool
     */
    public function getDelete()
    {
        return $this->delete;
    }

    /**
     * Get code
     *
     * @return \string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
    
    /**
     * Get type
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Get sort
     *
     * @return \int
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * Get dateUp
     *
     * @return \DateTime
     */
    public function getDateUp()
    {
        return $this->date_up;
    }

    /**
     * Get description
     *
     * @return FieldDescription
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Get child
     *
     * @return Field
     */
    public function getChild()
    {
        return $this->child;
    }

    /**
     * Get elements
     *
     * @return ArrayCollection
     */
    public function getElements()
    {
        return $this->elements;
    }
    /*END GETTERS METHODS*/

    /*SETTER METHODS*/

    /**
     * Set document
     *
     * @return Field
     */
    public function setDocument(Document $document) {
        $this->document = $document;

        return $this;
    }

    /**
     * Set parent
     *
     * @return Field
     */
    public function setParent($field)
    {
        $this->parent = $field;

        return $this;
    }

    /**
     * Set group
     *
     * @return Field
     */
    public function setGroup($field)
    {
        $this->group = $field;

        return $this;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Field
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Set choice
     *
     * @param \int $choice
     *
     * @return Field
     */
    public function setChoice($choice)
    {
        $this->choice = $choice;

        return $this;
    }

    /**
     * Set delete
     *
     * @param \int $delete
     *
     * @return Field
     */
    public function setDelete($delete)
    {
        $this->delete = $delete;

        return $this;
    }

    /**
     * Set code
     *
     * @param \string $code
     * @return Field
     */
    public function setCode(Code $code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Field
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }
    
    /**
     * Set value
     *
     * @param string $value
     *
     * @return Field
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Set sort
     *
     * @param \int $sort
     *
     * @return Field
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Set dateUp
     *
     * @param \DateTime $dateUp
     *
     * @return Field
     */
    public function setDateUp($dateUp)
    {
        $this->date_up = $dateUp;
        return $this;
    }
    
    /**
     * Set description
     *
     * @return Field
     */
    public function setDescription(FieldDescription $description) {
        $this->description = $description;
        return $this;
    }
    
    /**
     * Add Elem
     *      
     * @return Field
     */
    public function addElem(Field $Field)
    {
        $this->elements[] = $Field;
        return $this;
    }
    
    /**
     * Add Child
     *      
     * @return Field
     */
    public function setChild(Field $Field)
    {
        $this->child = $Field;
        return $this;
    }

    /*END SETTER METHODS*/

    /*CONSTRUCT METHOD*/
    function __construct()
    {
        $this->date_up          = new \DateTime("now");
        $this->description      = new FieldDescription();
        $this->code             = new Code();
        $this->elements         = new ArrayCollection();
    }

    public function __toString()
    {
        if($this->getDescription()->getName()){
            return $this->getDescription()->getName();
        }
        return 'Field';
    }
    

}

