<?php

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Document;
use AppBundle\Entity\User;
/**
 * Order
 *
 * @ORM\Table(name="orders")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OrderRepository")
 */
class Order
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", nullable=true, type="integer", length=11)
     */
    private $user_id;

    /**
     * @var int
     *
     * @ORM\Column(name="document_id", nullable=true, type="integer", length=11)
     */
    private $document_id;

    /**
     * @var int
     *
     * @ORM\Column(name="account_id", nullable=true, type="integer", length=11)
     */
    private $account_id;

    /**
     * @var string
     *
     * @ORM\Column(name="merch_id", type="string", length=255)
     */
    private $merch_id;

    /**
     * @var string
     *
     * @ORM\Column(name="t_id", type="string", length=255)
     */
    private $t_id;
    
     /**
     * @var int
     *
     * @ORM\Column(name="result_code", nullable=true, type="integer", length=11)
     */
    private $result_code;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal", length=8)
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="rrn", nullable=true, type="string", length=255)
     */
    private $rrn;

    /**
     * @var string
     *
     * @ORM\Column(name="transmission_date_time", nullable=true, type="string", length=255)
     */
    private $transmissionDateTime;

    /**
     * @var string
     *
     * @ORM\Column(name="masked_pan", type="string", nullable=true, length=255)
     */
    private $maskedPan;

    /**
     * @var string
     *
     * @ORM\Column(name="cardholder", type="string", nullable=true, length=255)
     */
    private $cardholder;

    /**
     * @var string
     *
     * @ORM\Column(name="authcode", type="string", nullable=true, length=255)
     */
    private $authcode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="is_fully_authenticated", nullable=true, type="datetime")
     */
    private $isFullyAuthenticated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ts", nullable=true, type="datetime")
     */
    private $ts;

    /**
     * @var string
     *
     * @ORM\Column(name="signature", nullable=true, type="string", length=255)
     */
    private $signature;
    
    /**
     * One Order have One Document.
     * @ORM\OneToOne(targetEntity="Document", inversedBy="order")
     * @ORM\JoinColumn(name="document_id", referencedColumnName="id")
     */
    private $document;
    
    /**
     * Many Orders have One User.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="orders")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return Order
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set documentId
     *
     * @param integer $documentId
     *
     * @return Order
     */
    public function setDocumentId($documentId)
    {
        $this->document_id = $documentId;

        return $this;
    }

    /**
     * Get documentId
     *
     * @return int
     */
    public function getDocumentId()
    {
        return $this->document_id;
    }

    /**
     * Set accountId
     *
     * @param integer $accountId
     *
     * @return Order
     */
    public function setAccountId($accountId)
    {
        $this->account_id = $accountId;

        return $this;
    }

    /**
     * Get accountId
     *
     * @return int
     */
    public function getAccountId()
    {
        return $this->account_id;
    }

    /**
     * Set merchId
     *
     * @param integer $merchId
     *
     * @return Order
     */
    public function setMerchId($merchId)
    {
        $this->merch_id = $merchId;

        return $this;
    }

    /**
     * Get merchId
     *
     * @return int
     */
    public function getMerchId()
    {
        return $this->merch_id;
    }

    /**
     * Set tId
     *
     * @param integer $tId
     *
     * @return Order
     */
    public function setTId($tId)
    {
        $this->t_id = $tId;

        return $this;
    }

    /**
     * Get tId
     *
     * @return int
     */
    public function getTId()
    {
        return $this->t_id;
    }

    /**
     * Set amount
     *
     * @param string $amount
     *
     * @return Order
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }
    
    /**
     * Set result_code
     *
     * @param int $resultCode
     *
     * @return Order
     */
    public function setResultCode($resultCode)
    {
        $this->result_code = $resultCode;

        return $this;
    }

    /**
     * Get result_code
     *
     * @return int
     */
    public function getResultCode()
    {
        return $this->result_code;
    }

    /**
     * Set rrn
     *
     * @param string $rrn
     *
     * @return Order
     */
    public function setRrn($rrn)
    {
        $this->rrn = $rrn;

        return $this;
    }

    /**
     * Get rrn
     *
     * @return string
     */
    public function getRrn()
    {
        return $this->rrn;
    }

    /**
     * Set transmissionDateTime
     *
     * @param string $transmissionDateTime
     *
     * @return Order
     */
    public function setTransmissionDateTime($transmissionDateTime)
    {
        $this->transmissionDateTime = $transmissionDateTime;

        return $this;
    }

    /**
     * Get transmissionDateTime
     *
     * @return string
     */
    public function getTransmissionDateTime()
    {
        return $this->transmissionDateTime;
    }

    /**
     * Set maskedPan
     *
     * @param string $maskedPan
     *
     * @return Order
     */
    public function setMaskedPan($maskedPan)
    {
        $this->maskedPan = $maskedPan;

        return $this;
    }

    /**
     * Get maskedPan
     *
     * @return string
     */
    public function getMaskedPan()
    {
        return $this->maskedPan;
    }

    /**
     * Set cardholder
     *
     * @param string $cardholder
     *
     * @return Order
     */
    public function setCardholder($cardholder)
    {
        $this->cardholder = $cardholder;

        return $this;
    }

    /**
     * Get cardholder
     *
     * @return string
     */
    public function getCardholder()
    {
        return $this->cardholder;
    }

    /**
     * Set authcode
     *
     * @param string $authcode
     *
     * @return Order
     */
    public function setAuthcode($authcode)
    {
        $this->authcode = $authcode;

        return $this;
    }

    /**
     * Get authcode
     *
     * @return string
     */
    public function getAuthcode()
    {
        return $this->authcode;
    }

    /**
     * Set isFullyAuthenticated
     *
     * @param \DateTime $isFullyAuthenticated
     *
     * @return Order
     */
    public function setIsFullyAuthenticated($isFullyAuthenticated)
    {
        $this->isFullyAuthenticated = $isFullyAuthenticated;

        return $this;
    }

    /**
     * Get isFullyAuthenticated
     *
     * @return \DateTime
     */
    public function getIsFullyAuthenticated()
    {
        return $this->isFullyAuthenticated;
    }

    /**
     * Set ts
     *
     * @param \DateTime $ts
     *
     * @return Order
     */
    public function setTs($ts)
    {
        $this->ts = $ts;

        return $this;
    }

    /**
     * Get ts
     *
     * @return \DateTime
     */
    public function getTs()
    {
        return $this->ts;
    }

    /**
     * Set signature
     *
     * @param string $signature
     *
     * @return Order
     */
    public function setSignature($signature)
    {
        $this->signature = $signature;

        return $this;
    }

    /**
     * Get signature
     *
     * @return string
     */
    public function getSignature()
    {
        return $this->signature;
    }
    
    /**
     * Set document
     *
     * @return Order
     */
    public function setDocument(Document $Document)
    {
        $this->document = $Document;

        return $this;
    }

    /**
     * Get document
     *
     * @return string
     */
    public function getDocument()
    {
        return $this->document;
    }
    
    /**
     * Set user
     *
     * @return Order
     */
    public function setUser(User $User)
    {
        $this->user = $User;

        return $this;
    }

    /**
     * Get user
     *
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }
    
    public function __construct() {
        $this->t_id = md5(uniqid(null, true));
    }
    
}

