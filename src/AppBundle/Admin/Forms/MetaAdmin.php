<?php
namespace AppBundle\Admin\Forms;

use AppBundle\AppBundle;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AppBundle\Entity\Meta;

class MetaAdmin extends AbstractAdmin
{

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('description.name', 'text', array ('label' => 'Имя'))
            ->add('slug', 'text', array ('label' => 'URL алиас'))
            ->add('_action', null, array('label'=>'Действия',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('description.name', 'text', array ('label' => 'Имя'))
            ->add('slug', 'text', array ('label' => 'URL алиас'))
            ->add('description.description', 'textarea', array('attr' => array('class'=>'ckeditor'), 'label'=>'Описание', 'empty_data' => ""))
            ->add('description.meta_h1', 'text', array ('label' => 'Заголовок первого уровня', 'required' => false, 'empty_data' => ""))
            ->add('description.meta_title', 'text', array ('label' => 'Заголовок окна страницы', 'required' => false, 'empty_data' => ""))
            ->add('description.meta_description', 'textarea', array ('label' => 'Мета описание', 'required' => false, 'empty_data' => ""))
            ->add('description.meta_keyword', 'text', array ('label' => 'Ключевые слова', 'required' => false, 'empty_data' => "",))
        ;
    }


    public function toString($object)
    {
        return $object instanceof Meta
            ? $object->getDescription()->getName()
            : 'Meta'; // shown in the breadcrumb on the create view
    }

    public function prePersist($Object)
    {
        $this->modelManager->getEntityManager(Meta::class)->getRepository(Meta::class)->addMeta($Object);
    }

}