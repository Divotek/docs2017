<?php
namespace AppBundle\Admin\Forms;

use AppBundle\Entity\Document;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class DocumentAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
       
        $formMapper
            ->tab('Document', array('label'=>'Документ'))
                ->with('Content', array('class' => 'col-md-6 document_edit', 'label'=>'Описание'))
                    ->add('description.description', 'textarea', array('attr' => array('class'=>'ckeditor'), 'label'=>'Описание', 'empty_data' => ""))
                ->end()

                ->with('Fields', array('class' => 'col-md-6 document_meta', 'label'=>'Поля'))

                    ->add('fields', 'sonata_type_collection', array(
                        'type_options' => array(
                            // Prevents the "Delete" option from being displayed
                            'delete' => false,
                            'delete_options' => array(
                                // You may otherwise choose to put the field but hide it
                                'type'         => 'hidden',
                                // In that case, you need to fill in the options as well
                                'type_options' => array(
                                    'mapped'   => false,
                                    'required' => false,
                                ),

                            )
                        ),
                        'label' => 'Поля документа',
                        'btn_add'   => 'Добавить поле',
                    ), array(
                        'edit' => 'inline',
                        'inline' => 'standart',
                        'sortable' => 'sort',
                    ))

                ->end()
            ->end()

            ->tab('Meta', array('label'=>'Настройки'))
                ->with('Meta', array('class' => 'col-md-7', 'label'=>'Общие'))
                    ->add('description.name', 'text', array ('label' => 'Имя'))
                    ->add('price', 'text', array ('label' => 'Цена документа грн.'))
                    ->add('sort', 'number', array ('label' => 'Сортировка'))
                    ->add('slug', 'text', array('label'=> 'URL алиас'))
                    ->add('description.short_description', 'textarea', array('attr' => array('class'=>'ckeditor'), 'label'=>'Короткое описание', 'empty_data' => "", 'required' => false))
                    ->add('description.meta_h1', 'text', array ('label' => 'Заголовок первого уровня', 'required' => false, 'empty_data' => ""))
                    ->add('description.meta_title', 'text', array ('label' => 'Заголовок окна страницы', 'required' => false, 'empty_data' => ""))
                    ->add('description.meta_description', 'textarea', array ('label' => 'Мета описание', 'required' => false, 'empty_data' => ""))
                    ->add('description.meta_keyword', 'text', array ('label' => 'Ключевые слова', 'required' => false, 'empty_data' => "",))
                ->end()
                ->with('Cats', array('class' => 'col-md-5', 'label'=>'Категории'))
                    ->add('categories', 'sonata_type_model',
                        array(
                            'expanded' => true, 'multiple' => true, 'label'=>'Отображать в категориях'
                        )
                    )
                ->end()
            ->end();
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('description.name', 'text', array ('label' => 'Имя'))
            ->add('sort', 'text', array ('label' => 'Сортировка'))
            ->add('user.username', 'text', array ('label' => 'Имя пользователя'))
            ->add('type', 'text', array ('label' => 'Тип'))
            ->add('slug', 'text', array ('label' => 'URL алиас'))
            ->add('date_up', 'date', array ('label' => 'Дата обновления', 'format' => 'd:m:Y H:i'))
            ->add('status', 'choice', array(
                'editable' => true,
                'choices' => array(
                    1 => 'Опубликовано',
                    2 => 'Отключено',
                ),
                'label' => 'Статус'
            ))
            ->add('_action', null, array('label'=>'Действия',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }


    public function toString($object)
    {
        return $object instanceof Document
            ? $object->getDescription()->getName()
            : 'Document'; // shown in the breadcrumb on the create view
    }

    protected $datagridValues = array(

        '_sort_by' => 'sort',
    );

    public function prePersist($Object)
    {
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $Object->setUser($user);
        $Object->setType('admin');
        $this->modelManager->getEntityManager(Document::class)->getRepository(Document::class)->addDocument($Object);
    }

    public function preUpdate($Object)
    {
        $Object->setDateUp(new \DateTime("now"));
        $this->modelManager->getEntityManager(Document::class)->getRepository(Document::class)->updateDocument($Object);
    }
    
    /*public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->andWhere(
            $query->expr()->eq($query->getRootAlias() . '.type', ':role')
        );
        $query->setParameter('role', 'admin');
        return $query;
    }*/

}