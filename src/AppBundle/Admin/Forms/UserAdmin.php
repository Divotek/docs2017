<?php
namespace AppBundle\Admin\Forms;

use AppBundle\AppBundle;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use AppBundle\Entity\User;
use AppBundle\Entity\Role;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sonata\AdminBundle\Route\RouteCollection;

class UserAdmin extends AbstractAdmin
{

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('username', 'text', array ('label' => 'Логин'))
            ->add('string_roles', 'text', array('label'=>'Роль в системе'))
            ->add('date_up', 'date', array ('label' => 'Дата создания', 'format' => 'd:m:Y H:i'))
            ->add('_action', null, array('label'=>'Действия',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $DataRoles = $this->modelManager->getEntityManager(Role::class)->createQuery('SELECT r FROM AppBundle:Role r')->getResult();
        $roles = [];
        
        foreach ($DataRoles as $Role){
            $roles[$Role->getName()] = $Role->getId();
        }
        
        $ChoiceRoles = $this->getSubject()->getFormRoles();
        
        $formMapper
        ->with('Content', array('class' => 'col-md-9', 'label'=>'Описание'))
            ->add('username', 'text', array ('label' => 'Логин'))            
            ->add('email', 'email', array ('label' => 'Почтовый адрес', 'required'=>false))            
            ->add('plainPassword', 'hidden', array ('label' => 'Пароль', 'required'=>false, 'empty_data' => '_'))            
            //->add('password', 'password', array ('label' => 'Пароль'))            
            ->add('changed', ChoiceType::class,  array(
                'multiple' => true,
                'choices' => $roles,
                'data'    => $ChoiceRoles,
                //'mapped' => false,
                'by_reference'=>false
            ))
        ->end();
    }
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
    }
   
    public function preUpdate($Object)
    {
        $this->modelManager->getEntityManager(User::class)->getRepository(User::class)->upUser($Object);
    }

    public function toString($object)
    {
        return $object instanceof User
            ? $object->getUserName()
            : 'User'; // shown in the breadcrumb on the create view
    }

    

}