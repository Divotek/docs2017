<?php
namespace AppBundle\Admin\Forms;

use AppBundle\AppBundle;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AppBundle\Entity\Page;
use AppBundle\Entity\Category;

class PageAdmin extends AbstractAdmin
{

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('description.name', 'text', array ('label' => 'Имя'))
            ->add('sort', 'text', array ('label' => 'Сортировка', 'editable'=>true))
            ->add('type_list', 'text', array ('label' => 'Тип'))
            ->add('parent.description.name', 'text', array ('label' => 'Родительский'))
            ->add('date_up', 'date', array ('label' => 'Дата обновления', 'format' => 'd:m:Y H:i'))
            ->add('status', 'choice', array(
                'editable' => true,
                'choices' => array(
                    1 => 'Опубликована',
                    2 => 'Отключена',
                ),
                'label' => 'Статус'
            ))
            ->add('_action', null, array('label'=>'Действия',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
        ->with('Content', array('class' => 'col-md-9', 'label'=>'Описание'))
            ->add('description.name', 'text', array ('label' => 'Имя'))
            ->add('slug', 'text', array('label'=> 'URL алиас'))
            ->add('sort', 'number', array ('label' => 'Сортировка'))
            ->add('description.description', 'textarea', array('attr' => array('class'=>'ckeditor'), 'label'=>'Описание', 'empty_data' => ""))
        ->end()

        ->with('Meta', array('class' => 'col-md-3', 'label'=>'Мета данные'))
            ->add('showMain', 'checkbox', array('label'=> 'Отображать верхнем уровне главного меню', 'required' => false))
            ->add('showFooter', 'checkbox', array('label'=> 'Отображать в подвале сайта', 'required' => false))
            ->add('parent', 'sonata_type_model', array(
                'required' => false,
                'placeholder' => 'Нет',
                'empty_data' => NULL,
                'class'     => 'AppBundle\Entity\Page',
                'property'  => 'description.name',
                'label'     => 'Родительская',
                'btn_add'   => false
            ))
            ->add('type_list', ChoiceType::class, array(
                'choices' => array('Ссылка' => "link", 'Список' => "drop"), 'label'=>'Тип страницы'))
            ->add('category', 'sonata_type_model', array(
                'required' => false,
                'placeholder' => 'Нет',
                'empty_data' => NULL,
                'class'     => 'AppBundle\Entity\Category',
                'property'  => 'description.name',
                'label'     => 'Выводить категорию',
                'btn_add'   => false
            ))
            ->add('description.meta_h1', 'text', array ('label' => 'Заголовок первого уровня', 'required' => false, 'empty_data' => ""))
            ->add('description.meta_title', 'text', array ('label' => 'Заголовок окна страницы', 'required' => false, 'empty_data' => ""))
            ->add('description.meta_description', 'textarea', array ('label' => 'Мета описание', 'required' => false, 'empty_data' => ""))
            ->add('description.meta_keyword', 'text', array ('label' => 'Ключевые слова', 'required' => false, 'empty_data' => "",))
        ->end();

    }

    protected $datagridValues = array(

        '_sort_by' => 'sort',
    );

    public function toString($object)
    {
        return $object instanceof Page
            ? $object->getDescription()->getName()
            : 'Page'; // shown in the breadcrumb on the create view
    }

    public function prePersist($Object)
    {
        $this->modelManager->getEntityManager(Page::class)->getRepository(Page::class)->addPage($Object);
    }

    public function preUpdate($Object)
    {
        $Object->setDateUp(new \DateTime("now"));
    }

}