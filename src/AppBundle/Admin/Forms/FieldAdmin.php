<?php
namespace AppBundle\Admin\Forms;

use AppBundle\AppBundle;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AppBundle\Entity\Field;

class FieldAdmin extends AbstractAdmin
{

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('description.name', 'text', array ('label' => 'Имя'))
            ->add('sort', 'text', array ('label' => 'Сортировка'))
            ->add('date_up', 'date', array ('label' => 'Дата обновления', 'format' => 'd:m:Y H:i'))
            ->add('status', 'choice', array(
                'editable' => true,
                'choices' => array(
                    1 => 'Опубликовано',
                    2 => 'Отключено',
                ),
                'label' => 'Статус'
            ))
            ->add('_action', null, array('label'=>'Действия',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ))

        ;

    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        if($this->getRequest()->get('id'))
        {
            $document_id = $this->getRequest()->get('id');
        } 
        else {
            $document_id = $this->getRequest()->get('objectId');
        }
        
        $query = $this->modelManager->getEntityManager(Field::class)->createQuery('SELECT f FROM AppBundle:Field f WHERE f.document_id = :id');
        $query->setParameter('id',$document_id);
        
        $formMapper
        ->with('Настройки')
            ->add('sort', 'hidden', array ('label' => 'Сорт',  'required' => false))
            ->add('code.value', 'text', array ('label' => 'Код для вставки', 'required' => false))
            ->add('description.name', 'text', array ('label' => 'Имя', 'required' => true))
            ->add('type', ChoiceType::class, array
                (
                    'required' => true,
                    'choices' => array
                    (
                        'Ввод текста'   => "text",
                        'Ввод даты'     => "date",
                        'Ввод числа'    => 'number',
                        'Радио список'  => 'radio_list',
                        'Радио элемент' => 'radio_el',
                        'Чекбокс'       => 'checkbox',
                        'Разделитель'   => 'del'
                    ),
                    'label'         =>'Тип поля'
                )
            )    
            ->add('parent', 'sonata_type_model', array(
                'required' => false,
                'placeholder' => 'Нет',
                'empty_data' => NULL,
                'query' => $query,
                'property'  => 'description.name',
                'label'     => 'Зависимый',
                'btn_add'   => false
            ))
            ->add('group', 'sonata_type_model', array(
                'required' => false,
                'query' => $query,
                'placeholder' => 'Нет',
                'empty_data' => NULL,
                'property'  => 'description.name',
                'label'     => 'Групировка',
                'btn_add'   => false
            ))
            
            ->add('choice', 'checkbox', array ('label' => 'Активный', 'required' => false))
            ->add('delete', 'checkbox', array ('label' => 'Удалить', 'required' => false))
        ->end()

        ->with('Описание')
            ->add('description.description', 'textarea', array('label'=>'Описание', 'empty_data' => "", 'required' => false))
        ->end();

    }

    protected $datagridValues = array(

        '_sort_by' => 'sort',
    );

    public function toString($object)
    {
        return $object instanceof Field
            ? $object->getDescription()->getName()
            : 'Field'; // shown in the breadcrumb on the create view
    }

    public function prePersist($Object)
    {
        $this->modelManager->getEntityManager(Field::class)->getRepository(Field::class)->addField($Object);
    }

    public function preUpdate($Object)
    {
        $Object->setDateUp(new \DateTime("now"));
    }
    
}