<?php
namespace AppBundle\Admin\Forms;

use AppBundle\AppBundle;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use AppBundle\Entity\Setting;

class SettingAdmin extends AbstractAdmin
{

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name', 'text', array ('label' => 'Имя'))
            ->add('slug', 'text', array ('label' => 'URL алиас'))
            ->add('status', 'choice', array(
            'editable' => true,
            'choices' => array(
                1 => 'Включено',
                2 => 'Отключено',
            ),
            'label' => 'Статус'
            ))
            ->add('_action', null, array('label'=>'Действия',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('slug', array('class' => 'col-md-4', 'label'=>'Системное имя'))   
                ->add('slug', 'text', array ('label' => ' '))
            ->end()
            ->with('name', array('class' => 'col-md-4', 'label'=>'Описание'))   
                ->add('name', 'text', array ('label' => ' ', 'attr' => []))                
            ->end()            
            ->with('value', array('class' => 'col-md-4', 'label'=>'Значение'))   
                ->add('value', 'text', ['label' => ' ', 'required' => false,])
            ->end()
        ;
    }


    public function toString($object)
    {
        return $object instanceof Setting
            ? $object->getName()
            : 'Setting'; // shown in the breadcrumb on the create view
    }


}