<?php
namespace AppBundle\Admin\Forms;

use AppBundle\Entity\Category;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class CategoryAdmin extends AbstractAdmin
{

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Content', array('class' => 'col-md-9', 'label'=>'Описание'))
                ->add('description.name', 'text', array ('label' => 'Имя'))
                ->add('sort', 'number', array ('label' => 'Сортировка'))
                ->add('description.description', 'textarea', array('attr' => array('class'=>'ckeditor'), 'label'=>'Описание', 'empty_data' => ""))
            ->end()

            ->with('Meta', array('class' => 'col-md-3', 'label'=>'Мета данные'))
                ->add('slug', 'text', array('label'=> 'URL алиас'))
                ->add('parent', 'sonata_type_model', array(
                    'required' => false,
                    'placeholder' => 'Нет',
                    'empty_data' => NULL,
                    'class'     => 'AppBundle\Entity\Category',
                    'property'  => 'description.name',
                    'label'     => 'Родительская',
                    'btn_add'   => 'Создать новую'
                ))
                ->add('type_list', ChoiceType::class, array(
                    'choices' => array('Ссылка' => "index", 'Список' => "drop", 'Блог категорий' => "blog"), 'label'=>'Тип категории'))
                ->add('description.meta_h1', 'text', array ('label' => 'Заголовок первого уровня', 'required' => false, 'empty_data' => ""))
                ->add('description.meta_title', 'text', array ('label' => 'Заголовок окна страницы', 'required' => false, 'empty_data' => ""))
                ->add('description.meta_description', 'textarea', array ('label' => 'Мета описание', 'required' => false, 'empty_data' => ""))
                ->add('description.meta_keyword', 'text', array ('label' => 'Ключевые слова', 'required' => false, 'empty_data' => "",))
            ->end();

    }


    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('description.name', 'text', array ('label' => 'Имя'))
            ->add('sort', 'text', array ('label' => 'Сортировка'))
            ->add('slug', 'text', array ('label' => 'URL алиас'))
            ->add('date_up', 'date', array ('label' => 'Дата обновления', 'format' => 'd:m:Y H:i'))
            ->add('status', 'choice', array(
                'editable' => true,
                'choices' => array(
                    1 => 'Опубликовано',
                    2 => 'Отключено',
                ),
                'label' => 'Статус'
            ))
            ->add('_action', null, array('label'=>'Действия',
                'actions' => array(
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    public function toString($object)
    {
        return $object instanceof Category
            ? $object->getDescription()->getName()
            : 'Category'; // shown in the breadcrumb on the create view
    }

    protected $datagridValues = array(

        '_sort_by' => 'sort',
    );

    public function prePersist($Object)
    {
        $this->modelManager->getEntityManager(Category::class)->getRepository(Category::class)->addCategory($Object);
    }

    public function preUpdate($Object)
    {
        $Object->setDateUp(new \DateTime("now"));
    }
}