<?php
namespace AppBundle\Admin\Forms;

use AppBundle\AppBundle;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use AppBundle\Entity\Role;
use Sonata\AdminBundle\Route\RouteCollection;
class RoleAdmin extends AbstractAdmin
{

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name', 'text', array ('label' => 'Имя'))
            ->add('role', null, array('label'=>'Роль в системе'))
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper      
            ->add('name', 'text', array ('label' => 'Имя'))            
            ->add('users', 'sonata_type_model',
                array(
                    'expanded' => true, 'multiple' => true, 'label'=>'Пользователи', 'btn_add' => false
                )
            )
        ->end();
    }
    
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
        $collection->remove('create');
    }
    
    public function toString($object)
    {
        return $object instanceof Role
            ? $object->getName()
            : 'Role'; // shown in the breadcrumb on the create view
    }

    

}