<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Category;

class CategoryController extends Controller
{

    /**
     * @Route("/category/{slug}", name="category_show" , requirements={"slug": "^[0-9a-zA-Z\-]{1,200}$"})
     */
    public function showAction($slug = 1)
    {
        $Category = $this->getDoctrine()
            ->getRepository(Category::class)
            ->getCategory($slug);
        //dump($Category->getChildren()->toArray()); die();
        return $this->render('Category/' . $Category->getTypeList() . '.html.twig', ['Category' => $Category]);
    }
    
}
