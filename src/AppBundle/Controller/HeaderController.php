<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Page;

class HeaderController extends Controller
{

    public function indexAction($Meta)
    {
        $Model = $this->getDoctrine()->getRepository(Page::class);
        $links = $Model->getLinks();
        return $this->render('AppBundle:Main:header.html.twig', [ 'links' => $links, 'Meta'=> $Meta]);
    }
}
