<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Page;

class PageController extends Controller
{

    /**
     * @Route("/page/{slug}", name="page_show" , requirements={"slug": "^[0-9a-zA-Z\-]{1,200}$"})
     */
    public function showAction($slug = 1)
    {
        $Page = $this->getDoctrine()
            ->getRepository(Page::class)
            ->getPage($slug);
        //dump($Page); die();
        return $this->render('AppBundle:Page:index.html.twig', ['Page' => $Page]);
    }
    
}
