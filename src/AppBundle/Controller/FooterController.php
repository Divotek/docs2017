<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Page;


class FooterController extends Controller
{
    public function indexAction()
    {
        $Model = $this->getDoctrine()->getRepository(Page::class);
        $links = $Model->getLinks();

        return $this->render('AppBundle:Main:footer.html.twig', [ 'links' => $links ]);
    }
}