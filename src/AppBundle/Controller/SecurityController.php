<?php

// src/AppBundle/Controller/SecurityController.php
namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use AppBundle\Entity\Meta;

use AppBundle\Form\LoginForm;

class SecurityController extends Controller
{

    public function loginAction()
    {
        /**
         * @Route("/login", name="login")
         */

        $authenticationUtils = $this->get('security.authentication_utils');
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        $Meta = $this->getDoctrine()
            ->getRepository(Meta::class)
            ->getMeta('login');


        return $this->render(
            'Login/login.html.twig',
            array(
                // last username entered by the user
                'last_username' => $lastUsername,
                'error'         => $error,
                'Meta'          => $Meta
            )
        );
    }

    /**
     * @Route("/logout")
     */
    public function logoutAction()
    {
        //throw new \RuntimeException('This shoud newer be called directly');
        return $this->redirectToRoute('login');
    }
    
}