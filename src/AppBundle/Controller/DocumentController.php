<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Document;

class DocumentController extends Controller
{
        
     /**
     * @Route("/document/add", name="document_add")
     */
    public function addAction(Request $request)
    {
            
        if($request->get('document_id') != NULL && $request->get('fields') != NULL)
        {
            $data['fields']         = $request->get('fields');
            $data['document_id']    = (int)$request->get('document_id');
            $data['user']           = $this->getUser();
            
            $Repository = $this->getDoctrine()->getRepository(Document::class);
                        
            $Document = $Repository->addDocumentUser($data);
            
            $content = $Repository->getContent($Document);
            
            $fileName = $Document->getSlug() . '_'  . md5(time()) . '_' . $Document->getId() . '.pdf';
            
            $this->get('knp_snappy.pdf')->generateFromHtml(
                $this->renderView('AppBundle:PDF:index.html.twig', ['content' => $content]), './app/pdf/' .$fileName                
            );
            
            $Document->setLink($fileName);
            $em = $this->getDoctrine()->getManager();
            $em->persist($Document);
            $em->flush();
        }
        return $this->redirectToRoute('personal');
    }

    /**
     * @Route("/document/{slug}", name="document_show" , requirements={"slug": "^[0-9a-zA-Z\-]{4,200}$"})
     */
    public function showAction($slug)
    {
        $Document = $this->getDoctrine()
            ->getRepository(Document::class)
            ->getDocument($slug);
        //dump($slug); die();
        return $this->render('AppBundle:Document:index.html.twig', ['Document' => $Document]);
    }
    
   
}
