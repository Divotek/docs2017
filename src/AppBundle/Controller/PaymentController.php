<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Meta;
use AppBundle\Entity\Order;
use AppBundle\Entity\Document;
use AppBundle\Entity\Setting;
use GuzzleHttp\Client;

class PaymentController extends Controller
{
    
    /**
     * @Route("/payment/{id}", name="payment", requirements={"id": "^[0-9]{1,}$"})
     */
    public function PaymentAction($id)
    {
        $em         = $this->getDoctrine()->getManager();
        $Document   = $em->find(Document::class, $id);
        $User       = $this->getUser();
        $Order      = new Order();
        $Order->setUser($User);
        $Order->setDocument($Document);
        $Order->setAmount($Document->getPrice());
        $Merch = $em->getRepository(Setting::class)->findOneBy(['slug' => 'merch_id']);
        
        $Order->setMerchId($Merch->getValue());
        dump($Order); die();
        $em->persist($Order);
        $em->flush();
        
        $this->RegisterPaymentAction($request);
        
       // dump($Order); die();
       
         /*
        $client = new Client();
        
       $response = $client->request(
            'GET', 
            'https://pps03.fuib.com/payment/start.wsm',
            [
                'query' => [
                    'mearch_id'     => $Order->getMerchId(),
                    'lang'          => 'ru',
                    'back_url_s'    => $this->generateUrl('order_success'),
                    'back_url_f'    => $this->generateUrl('order_failed'),
                    'o.user_id'     => $Order->getUser()->getId(),
                    'o.order_id'    => $Order->getId(),
                    'o.amount'      => $Order->getAmount(),
                    'o.t_id'        => $Order->getTId(),
                    
                ]
            ]
        );*/
    }

    /**
     * @Route("/register-payment", name="register_payment")
     */
    public function RegisterPaymentAction(Request $request)
    {
        dump('RegisterPaymentAction'); die();
    }
    
    /**
     * @Route("/check-payments-avail", name="check_payments_avail")
     */
    public function CheckPaymentsAvailAction(Request $request)
    {
        dump('CheckPaymentsAvailAction'); die();
    }
    
    /**
     * @Route("/order-failed", name="order_failed")
     */
    public function OrderFailedAction()
    {
        $Meta = $this->getDoctrine()->getManager()->getRepository(Meta::class)->getMeta('order-failed');
        
        return $this->render('Payment/order-failed.html.twig', ['Meta' => $Meta ]);
    }
    
    /**
     * @Route("/order-success", name="order_success")
     */
    public function OrderSuccessAction()
    {
        $Meta = $this->getDoctrine()->getManager()->getRepository(Meta::class)->getMeta('order-success');
        
        return $this->render('Payment/order-success.html.twig', ['Meta' => $Meta ]);
    }
    
}
