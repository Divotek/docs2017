<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Form\ChangePasswordType;
use AppBundle\Form\ChangeEmailType;
use AppBundle\Form\Model\ChangePasswordModel;
use AppBundle\Form\Model\ChangeEmailModel;
use AppBundle\Entity\Document;
use AppBundle\Entity\Meta;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class PersonalController extends Controller
{
    private $PForm;
    private $EForm;

    /**
     * @Route("/personal", name="personal")
     */
    public function indexAction()
    {        
        if(!$this->getUser()){
            return $this->redirectToRoute('login');
        }
        
        $Meta = $this->getDoctrine()
            ->getRepository(Meta::class)
            ->getMeta('personal');
        
        $User = $this->getUser();
        
        $documets = $this->getDoctrine()
            ->getRepository(Document::class)
            ->getPersonalDocuments($User);
        
        $this->setFormPass();
        $this->setFormEmail();
        
        return $this->render(
            'Personal/index.html.twig',
            array('Meta' => $Meta, 'documents' => $documets, 'PForm' => $this->PForm->createView(), 'EForm' => $this->EForm->createView())
        );
    }
    
    public function changePasswdAction(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $this->setFormPass();
        
        $this->PForm->handleRequest($request);
        
        $changed = false;
        if($this->PForm->isSubmitted() && $this->PForm->isValid()) 
        {
            $params = $request->request->all();   
            
            $User = $this->getUser();
            
            $encoded = $encoder->encodePassword($User, $params['change_password']['newPassword']['first']);
            
            $User->setPassword($encoded);
            
            $em = $this->getDoctrine()->getManager();
            
            $em->persist($User);
            $em->flush();
            $changed = true;
        }        
        
        return $this->render(
            'Personal/formpass.html.twig',
            array('PForm' => $this->PForm->createView(), 'changed' => $changed)
        );
    }
    
    public function changeEmailAction(Request $request)
    {
        $this->setFormEmail();
        
        $this->EForm->handleRequest($request);
        
        $changed = false;
        
        if($this->EForm->isSubmitted() && $this->EForm->isValid()) 
        {
            $params = $request->request->all();   
            
            $User = $this->getUser();
            
            $User->setEmail($params['change_email']['email']);
            
            $em = $this->getDoctrine()->getManager();
            
            $em->persist($User);
            $em->flush();
            $changed = true;
        }        
        
        return $this->render(
            'Personal/formemail.html.twig',
            array('form' => $this->EForm->createView(), 'changed' => $changed)
        );
    }
    
    private function setFormPass() 
    {
        $changePasswordModel = new ChangePasswordModel();                
        $this->PForm = $this->createForm(ChangePasswordType::class, $changePasswordModel, [ 'action' => $this->generateUrl('passchange')]);
    }
    
    private function setFormEmail() 
    {
        $changeEmailModel = new ChangeEmailModel();                
        $this->EForm = $this->createForm(ChangeEmailType::class, $changeEmailModel, [ 'action' => $this->generateUrl('emailchange')]);
    }
    
}
