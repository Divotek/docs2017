<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Meta;

class HomeController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        // replace this example code with whatever you need

        $Meta = $this->getDoctrine()
            ->getRepository(Meta::class)
            ->getMeta('home');

        return $this->render('AppBundle:Main:home.html.twig', ['Meta' => $Meta]);
        
    }
    
    
}
